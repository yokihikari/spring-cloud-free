package com.yoki.free.api;

import com.platform.result.Result;
import com.yoki.free.api.dto.ApiUserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @ClassName IUserFeign
 * @Description TODO
 * @Author yoki
 * @Date 2021-01-11 14:23
 * @Vsersion 1.0
 */
@FeignClient(name = "user-server")
public interface IUserFeign {
    /**
     * OpenFeign测试Get
     * @return
     */
    @GetMapping("/user/api/by/id")
    Result<Object> queryUserById(@RequestParam("id") Long id);

    /**
     * OpenFeign测试Post
     * @return
     */
    @PostMapping("/user/api/by/dto")
    Result<ApiUserDTO> queryUserDto(@RequestBody ApiUserDTO apiUserDTO);

    /**
     * OpenFeign测试Ribbon负载均衡功能
     * @return
     */
    @GetMapping("/user/api/ribbon")
    Result<String> testRibbon();
}
