package com.yoki.free.base.controller;

import com.platform.result.Result;
import com.yoki.free.api.IUserFeign;
import com.yoki.free.api.dto.ApiUserDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @ClassName BaseController
 * @Description TODO
 * @Author yoki
 * @Date 2021-01-11 15:12
 * @Vsersion 1.0
 */
@RestController
@RequestMapping(value = "base")
@Api(tags = "内部调用Feign")
@RefreshScope
public class BaseController {
    @Autowired
    private IUserFeign userFeign;

    @GetMapping(value = "api/by/id")
    @ApiOperation(value = "Fegin Get调用测试接口")
    public Result queryUserById(@RequestParam("id") Long id) {
        return Result.data(userFeign.queryUserById(id));
    }

    @PostMapping(value = "api/by/dto")
    @ApiOperation(value = "Fegin Post调用测试接口")
    public Result queryUserDto(@Valid @RequestBody ApiUserDTO userDTO) {
        return Result.data(userFeign.queryUserDto(userDTO));
    }

    @PostMapping(value = "api/ribbon")
    @ApiOperation(value = "Ribbon调用测试接口")
    public Result testRibbon() {
        return Result.data(userFeign.testRibbon());
    }
}
