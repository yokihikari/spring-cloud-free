package com.yoki.free.base;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @ClassName BaseApplication
 * @Description base-启动类
 * @Author yoki
 * @Date 2021-01-11 15:18
 * @Vsersion 1.0
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.yoki"})
@ComponentScan(basePackages = {"com.yoki", "com.platform.exception"})
@MapperScan("com.yoki.*.*.mapper")
public class BaseApplication {
    public static void main(String[] args) {
        SpringApplication.run(BaseApplication.class, args);
    }
}
