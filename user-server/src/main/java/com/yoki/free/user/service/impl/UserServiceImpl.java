package com.yoki.free.user.service.impl;

import com.platform.exception.BusinessException;
import com.yoki.free.user.service.IUserService;
import org.springframework.stereotype.Service;

/**
 * @ClassName UserServiceImpl
 * @Description TODO
 * @Author yoki
 * @Date 2020-12-29 15:08
 * @Vsersion 1.0
 */
@Service
public class UserServiceImpl implements IUserService {

    public String exception() {
        throw new BusinessException("自定义异常");
    }
}
