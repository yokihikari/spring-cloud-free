package com.yoki.free.user.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @ClassName PostRequest
 * @Description TODO
 * @Author yoki
 * @Date 2020-12-30 10:33
 * @Vsersion 1.0
 */
@Data
public class PostRequest implements Serializable {
    @ApiModelProperty(value = "名字", name = "name")
    @NotEmpty
    private String name;
    @ApiModelProperty(value = "数值", name = "value")
    @NotEmpty
    private String value;
}
