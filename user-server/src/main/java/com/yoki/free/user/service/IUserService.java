package com.yoki.free.user.service;

/**
 * @ClassName IUserService
 * @Description TODO
 * @Author yoki
 * @Date 2020-12-29 15:07
 * @Vsersion 1.0
 */
public interface IUserService {
    /**
     * 返回异常
     * @return
     */
    String exception();
}
