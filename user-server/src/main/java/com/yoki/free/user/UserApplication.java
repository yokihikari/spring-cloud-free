package com.yoki.free.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @ClassName UserApplication
 * @Description TODO
 * @Author yoki
 * @Date 2020-12-29 14:46
 * @Vsersion 1.0
 */
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.yoki", "com.platform.exception"})
@MapperScan("com.yoki.*.*.mapper")
@SpringBootApplication
public class UserApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }
}
